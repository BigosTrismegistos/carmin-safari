﻿var token = "csoLxtbFYA8AgSZhDMnJjqPR";
var url = "http://carmin.mgpm.pl/create";

safari.application.addEventListener('command', handleCommand, false);

function handleCommand(e) {
    if(e.command == "carmin-post") {
        var url = e.target.browserWindow.activeTab.url;
        post(url);
    }
}

function show_message(message){
    var options = {
        body: message,
        tag: 'carmin-notification'
    };    
    var notification = new Notification('CARMIN', options);
}

function post(page_url) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    var params = 'token=' + token+'&text="'+encodeURIComponent(page_url)+'"';
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    // Handle request state change events
    xhr.onreadystatechange = function() {
        // If the request completed
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var response = JSON.parse( xhr.responseText );
                if( response.status == 'error' ){
                    show_message(response.text);
                } else {
                    show_message("Link został dodany!")
                }
            } else {
                show_message("Link nie został dodany - spróbuj ponownie");
            }
        }
    };
    xhr.send(params);
}


//chrome.contextMenus.create({
//
//    title: "Wyslij do CARMIN",
//    contexts:["page", "link", "image", "video", "audio"],
//    onclick: post
//
//});

//chrome.browserAction.onClicked.addListener(function (tab) {
//    var params = {
//        'pageUrl': tab.url
//    };
//    post(params);
//});